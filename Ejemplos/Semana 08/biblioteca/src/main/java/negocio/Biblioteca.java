package negocio;

import estructuras.TSBArrayList;
import modelos.Libro;
import modelos.Prestamo;
import modelos.Socio;
import util.Validaciones;

import java.util.List;

import static util.Validaciones.verificarNoNulo;

public class Biblioteca {
    private List<Libro> libros;
    private List<Socio> socios;
    private List<Prestamo> prestamos;

    public Biblioteca() {
        libros = new TSBArrayList<Libro>();
        socios = new TSBArrayList<Socio>();
        prestamos = new TSBArrayList<Prestamo>();
    }

    public void agregarLibro(Libro libro) {
        verificarNoNulo(libro);
        libros.add(libro);
    }

    public void agregarSocio(Socio socio) {
        socios.add(socio);
    }
    
    public boolean prestarLibro(int dni, int codigo) {
        boolean resultado = false;
        Socio socio = buscarSocio(dni);
        Libro libro = buscarLibro(codigo);
        if (libro != null && socio != null && libro.isDisponible() {
            Prestamo p = new Prestamo(socio, libro);
            prestamos.add(p);
            libro.setDisponible(false);
        }
        return resultado;
    }

    private Socio buscarSocio(int dni) {
        Socio resultado = null;
        for(Socio socio: socios) {
            if (socio.getDni() == dni) {
                resultado = socio;
                break;
            }
        } 
        return resultado;
    }    
    
    private Libro buscarLibro(int codigo) {
        Libro resultado = null;
        for(Libro libro: libros) {
            if (libro.getCodigo() == codigo) {
                resultado = libro;
                break;
            }
        } 
        return resultado;
    }


}
