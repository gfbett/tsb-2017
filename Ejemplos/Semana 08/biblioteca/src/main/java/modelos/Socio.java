package modelos;

public class Socio {
    private int dni;
    private String nombre;

    public Socio(int dni, String nombre) {
        this.dni = dni;
        this.nombre = nombre;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Socio{" +
                "dni=" + dni +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
