import tsb.estructuras.TSBHashMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by federico on 03/10/17.
 */
public class Principal {

    public static void main(String[] args) {

        Persona a = new Persona(123, "Pepe");
        Persona b = new Persona(234, "Jorge");

        Map<Integer, Persona> m = new TSBHashMap<>();
        m.put(a.getDni(), a);
        m.put(b.getDni(), b);

        System.out.println(m.get(123));
        System.out.println(m.get(124));
        System.out.println(m.get(234));

    }
}
