package tsb.estructuras;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TSBHashMap<K, V> implements Map<K, V>{

    private double maxLoadFactor;
    private List<Entry<K, V>>[] values;
    private int size;


    public TSBHashMap() {
        maxLoadFactor = 0.5;
        values = new List[13];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        int index = hash(key);
        List<Entry<K, V>> bucket = values[index];
        if (bucket == null) {
            return null;
        }
        Entry<K, V> entry = findEntry(bucket, key);
        return entry != null ? entry.getValue() : null;


    }

    @Override
    public V put(K key, V value) {
        V result = null;
        int index = hash(key);
        if (values[index] == null) {
            values[index] = new TSBArrayList<>();
        }
        List<Entry<K, V>> bucket = values[index];
        Entry<K, V> entry = findEntry(bucket, key);
        if (entry == null) {
            bucket.add(new TSBEntry<K, V>(key, value));
            size ++;
            if(loadFactor() > maxLoadFactor) {
                rehash();
            }
        } else {
            result = entry.setValue(value);
        }


        return result;
    }

    private void rehash() {

    }

    private double loadFactor() {
        return size / values.length;
    }

    private Entry<K, V> findEntry(List<Entry<K, V>> bucket, Object key) {
        for (Entry<K, V> e: bucket) {
            if (e.getKey().equals(key)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private int hash(Object key) {
        return key.hashCode() % values.length;
    }

    private static class TSBEntry<K, V> implements Map.Entry<K, V> {

        private K key;
        private V value;

        public TSBEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = value;
            this.value = value;
            return old;
        }
    }
}
