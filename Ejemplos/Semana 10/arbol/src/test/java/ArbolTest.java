import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArbolTest {

    private Arbol underTest;

    @Before
    public void setup() {
        underTest = new Arbol();
    }

    @Test
    public void testAdd() {
        underTest.add(1);
        assertEquals(1, underTest.size());
        underTest.add(3);
        assertEquals(2, underTest.size());
        underTest.add(2);
        assertEquals(3, underTest.size());
        underTest.add(4);
        assertEquals(4, underTest.size());


    }

}