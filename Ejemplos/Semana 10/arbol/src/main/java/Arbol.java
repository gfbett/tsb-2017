/**
 * Created by federico on 10/10/17.
 */
public class Arbol {

    private Nodo raiz;
    private int size;

    public Arbol() {
        raiz = null;
    }

    public int size() {
        return size;
    }

    public void add(int x) {
        Nodo p = raiz, q = null;
        while (p != null) {
            q = p;
            if (x < p.info) {
                p = p.izq;
            } else {
                p = p.der;
            }
        }
        Nodo n = new Nodo(x);
        if (q == null) {
            raiz = n;
        } else if(x < q.info) {
            q.izq = n;
        } else {
            q.der = n;
        }
        size ++;
    }

    public boolean contains(int x) {
        Nodo p = raiz;
        while (p != null && x != p.info) {
            if (x < p.info) {
                p = p.izq;
            } else {
                p = p.der;
            }
        }
        return p != null;
    }


    private static class Nodo {
        Nodo izq;
        Nodo der;
        int info;

        public Nodo(int info) {
            this.info = info;
        }
    }
}
